﻿using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ClarkCodingChallenge.Models;
using ClarkCodingChallenge.BusinessLogic;

namespace ClarkCodingChallenge.Controllers
{
    public class ContactsController : Controller
    {
        private readonly ApplicationDBContext _context;
        private ContactsService _contactsService;

        public ContactsController(ApplicationDBContext context, ContactsService contactsService)
        {
            _context = context;
            _contactsService = contactsService;

        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(Contact contact)
        {
            if (ModelState.IsValid)
            {
                string dbResult = _contactsService.NewContact(contact.FirstName, contact.LastName, contact.Email);
                if (dbResult == "Success")
                {
                    return View("Confirmation");
                }
                else
                {
                    ViewBag.ErrorMsg = dbResult;
                }

                return Index();
            }
            return View(contact);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ContactList(string LastName, bool Desc)
        {
            List<Contact> contacts;
            string lastNameTrimmed = LastName == null? "": LastName.Trim();

            contacts = _contactsService.GetContacts(lastNameTrimmed, Desc);

            return View(contacts);

        }

        [HttpGet]
        public IActionResult ContactList()
        {
            return ContactList("", false);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
