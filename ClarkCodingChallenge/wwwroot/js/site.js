﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function DoPost() {
    var requestVal = $('input[name="__RequestVerificationToken"]').val();
    var data = { "LastName": "", "Desc": false };
    data['__RequestVerificationToken'] = requestVal;

    $.ajax({
        type: "POST",
        url: "/Contacts/ContactList",
        data: data,
        success: function (response) {
            $("#container").html(response);
        }
    })
}
