﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ClarkCodingChallenge.Models;
using System.Net.Mail;

namespace ClarkCodingChallenge.BusinessLogic
{
    public class ContactsService
    {
        //TODO: Place business logic for contact here

        private static ApplicationDBContext _context;

        public ContactsService(ApplicationDBContext appContext)
        {
            _context = appContext;
        }

        public string NewContact(string firstName, string lastName, string email)
        {
            if (string.IsNullOrEmpty(firstName.Trim()))
                return "First Name is missing";
            if (string.IsNullOrEmpty(lastName.Trim()))
                return "Last name is missing";
            if (string.IsNullOrEmpty(email.Trim()) || !IsValidEmail(email.Trim()))
                return "Email is invalid";

            try
            {
                Contact contact = new Contact();
                contact.Email = email.Trim();
                contact.FirstName = firstName.Trim();
                contact.LastName = lastName.Trim();

                try
                {
                    _context.Contacts.Add(contact);
                    _context.SaveChanges();
                }
                catch (DbUpdateException e)
                {
                    var error = CheckHandleError(e);
                    if (error != null)
                    {
                        return error;
                    }
                    throw; //couldn’t handle that error, so rethrow
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                while(ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine(ex.StackTrace);

                return "Failed";
            }

            return "Success";
        }

        public List<Contact> GetContacts(string LastNameMatch, bool sortDesc = false)
        {
            List<Contact> result;
            if (string.IsNullOrEmpty(LastNameMatch.Trim()))
            {
                if (sortDesc)
                    result = _context.Contacts.OrderBy(item => item.LastName)
                                              .ThenBy(item => item.FirstName)
                                              .ToList();
                else
                    result = _context.Contacts.OrderByDescending(item => item.LastName)
                                              .ThenByDescending(item => item.FirstName)
                                              .ToList();
            }
            else
            {
                if (sortDesc)
                    result = _context.Contacts.Where(item => item.LastName == LastNameMatch.Trim())
                                              .OrderByDescending(item => item.LastName)
                                              .ThenByDescending(item => item.FirstName)
                                              .ToList();
                else
                    result = _context.Contacts.Where(item => item.LastName == LastNameMatch.Trim())
                                              .OrderBy(item => item.LastName)
                                              .ThenBy(item => item.FirstName)
                                              .ToList();
            }

            return result;
        }

        //public bool deleteContact(string email)
        //{

        //}
        //public bool UpdateContact(int id, string FirstName, string LastName, string Email)
        //{

        //}

        private string CheckHandleError(DbUpdateException e)
        {
            return e.Message;
        }
        private bool IsValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
               
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
