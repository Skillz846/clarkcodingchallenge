using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClarkCodingChallenge.Tests.BusinessLogicTests;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using ClarkCodingChallenge.BusinessLogic;
using ClarkCodingChallenge.Models;
using Xunit;

namespace ClarkCodingChallenge.Tests.BusinessLogicTests
{
    [TestClass]
    public class ContactsBusinessLogicTests
    {
        private static DbContextOptions<ApplicationDBContext> CreateTemporaryMemoryOptions()
        {
            var serviceProvider = new ServiceCollection()
                                        .AddEntityFrameworkInMemoryDatabase()
                                        .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<ApplicationDBContext>()
                                .UseInMemoryDatabase(databaseName: "test")
                                .UseInternalServiceProvider(serviceProvider);
            return builder.Options;

        }
        [TestMethod]
        [Fact]
        public void NewContactsTest_Success()
        {

            string resultReturned = "";
            using (var context = new ApplicationDBContext(CreateTemporaryMemoryOptions()))
            {
                var service = new ContactsService(context);
                resultReturned = service.NewContact("Test", "Last", "test@gmial.com");
            }

            Assert.AreEqual(resultReturned, "Success");
        }

        [TestMethod]
        [Fact]
        public void ListContactsTest_Success()
        {

            List<Contact> resultReturned;
            using (var context = new ApplicationDBContext(CreateTemporaryMemoryOptions()))
            {
                var service = new ContactsService(context);
                resultReturned = service.GetContacts("Monplaisir", true);
            }

            Assert.IsNotNull(resultReturned, "Test Passed");
        }
    }
}
